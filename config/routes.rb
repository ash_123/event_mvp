Rails.application.routes.draw do
 
  root "sessions#new"
  post 'sessions/login'
  get 'dashboard/' => 'dashboard#index'
  get 'sessions/authendicate'
  get 'sessions/logout'
  resources :users_roles
  resources :events, only: [:new, :edit, :destroy, :update, :create]
  resources :roles
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
