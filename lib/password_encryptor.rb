require "bcrypt"

class PasswordEncryptor

  include BCrypt
  attr_accessor :salt, :hash
  
  def initialize(secret, params={})
    @secret = secret
    @salt = params[:salt] || generate_new_salt
    @hash = nil
  end

  def generate_new_salt
    BCrypt::Engine.generate_salt
  end

  def generate_hash
    @hash = BCrypt::Engine.hash_secret(@secret, salt)
  end

  def salted
    generate_hash
    return self
  end
end