class User < ApplicationRecord
  mount_uploader :photo, AvatarUploader
  enum gender: { male: 0, female: 1 }

  before_create :assign_salt_and_hashed_password
  has_one :users_role
  has_one :role , through: :users_role

  private
  
  def assign_salt_and_hashed_password
    encrypted_password = PasswordEncryptor.new(self.password).salted
    self.salt = encrypted_password.salt
    self.password = encrypted_password.hash
  end
end
