class SessionsController < ApplicationController
  def new
  end
  def login
    user = User.find_by(email: params[:session][:email].downcase)
    if user && authenticate(user)
      session[:user_id] = user.id
      redirect_to dashboard_path
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def authenticate(user)
    return false unless user
    user.password == PasswordEncryptor.new(params[:session][:password], :salt => user.salt).salted.hash
  end

  def logout
    session.delete(:user_id)
    redirect_to root_url
  end
end
