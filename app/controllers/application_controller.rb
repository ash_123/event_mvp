class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  private
  def authenticate
    begin
       @current_user = User.find(session[:user_id])
    rescue Exception => e
      redirect_to root_path
    end
  end
end
