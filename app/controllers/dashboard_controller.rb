class DashboardController < ApplicationController
  before_action :authenticate
  def index
    @user = @current_user
    @events = Event.paginate(:page => params[:page])
  end
end
